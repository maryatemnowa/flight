'use strict';

let airlines =[];
let flight ={};

$(function() {
    $("#preload").hide();
    $("#result").hide();
    $("#myTab a").click(function(e){
        e.preventDefault();
        $(this).tab('show');
    });
    //get airlines
    $.ajax({
        type: 'GET',
        url: '/api/airlines',
        data: {},
        success: function(data) {
            airlines = JSON.parse(data);
        }
    });

    $('.date').datepicker({
        format: 'yyyy-mm-dd',
        startDate: '-0d'
    });

    $( "#search-form" ).submit(function(e) {
        e.preventDefault();
        clear();
        $("#preload").show();
        $("#result").hide();

        flight = new Flight($('#from-location').val(), $('#to-location').val(), moment($('#date').val()));
        flight.get_airport(flight.from.city).then(function(_data){
            let data = JSON.parse(_data);
            if(data && data.length){
                flight.from.airports = data;
                get_airport_to();
            }
            else{
                clear();
                $('#error-msg').append('No airport in the ' + flight.from.city +' city');
            }
        });

    });

});

function get_airport_to(){
    flight.get_airport(flight.to.city).then(function(_data){
        let data = JSON.parse(_data);
        if(data && data.length){
            flight.to.airports = data;
            search_flight();
        }
        else{
            clear();
            $('#error-msg').append('No airport in the ' + flight.to.city +' city');
        }
    });
}
function search_flight(){
    flight.search_flight(print_flight);
}

function print_flight(){
    $("#preload").hide();
    $("#result").show();
    flight.flights.forEach(function(item, num){
        if(moment(item.date).diff(moment(), 'day')<0)
            $('#day-' + num).addClass('hide');
        else {
            $('#day-' + num).append('<p>'+ item.date +'</p>');
            $('#panel-' + num).append('<table class="table" id="table-result-'+ num +'"> '+
                '<thead>'+
                '<tr>'+
                '<th>From</th>'+
                '<th>Time</th>'+
                '<th>To</th>'+
                '<th>Time</th>'+
                '<th>Price</th>'+
                '</tr>'+
                '</thead>'+
                '</table>');
            if(item.flights.length)
                airlines.forEach(function (airline){
                    $('#table-result-'+ num).append('<tr><th class="th-airline-name">'+ airline.name +'('+airline.code+')</th></tr>');
                    var flights = item.flights.filter(function(flight) {
                        return flight.airline.code == airline.code;
                    });
                    flights.forEach(function(flight){
                        $('#table-result-'+ num).append(
                            '<tr>' +
                            '<th><span class="country-name">'+ flight.start.countryName + '</span><br><span class="city-name">' + flight.start.cityName+ '</span><br>' + flight.start.airportName +'</th>' +
                            '<th>'+moment(flight.start.dateTime).format('HH:mm') + '</th>' +
                            '<th><span class="country-name">'+ flight.finish.countryName + '</span><br><span class="city-name">' + flight.finish.cityName+ '</span><br>' + flight.finish.airportName + '</th>' +
                            '<th>'+ moment(flight.finish.dateTime).format('HH:mm')+ '</th>' +
                            '<th> '+ flight.price + '</th>' +
                            '</tr>')
                    })
                });
            else $('#table-result-'+ num).append('No flights for you request');
        }

    })

}
function clear(){
    $("#preload").hide();
    $("#result").hide();
    flight ={};
    $('#error-msg').html('');
    $('.nav-tabs li a').html('');
    $('.nav-tabs li a').removeClass('hide');
    $('.tab-pane').html('');
}
