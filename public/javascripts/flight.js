'use strict';

class Flight {

    constructor(from, to, date) {
        this.from = {
            city: from,
            airports: []
        };
        this.to = {
            city: to,
            airports: []
        };
        this.date = date.format('YYYY-MM-DD');

        this.flights = [{
                date:  moment(date).subtract(2, 'day').format('YYYY-MM-DD'),
                flights: []
            },{
                date:  moment(date).subtract(1, 'day').format('YYYY-MM-DD'),
                flights: []
            },{
                date: date.format('YYYY-MM-DD'),
                flights: []
            },{
                date:  moment(date).add(1, 'day').format('YYYY-MM-DD'),
                flights: []
            },{
                date:  moment(date).add(2, 'day').format('YYYY-MM-DD'),
                flights: []
            }
        ];
    }
    set_from_airports(airports) {
        this.from.airports = airports;
    }
    set_to_airports(airports) {
        this.to.airports = airports;
    }
    get_airport(city_name) {
        let self = this;
        return $.ajax({
            type: 'GET',
            url: '/api/airports?q=' + city_name,
            data: {},
            success:
                function(airports){}
        });
    }

    search_flight(callback){
        let self = this;
        airlines.forEach(function (airline, num_airline){
            self.flights.forEach(function(date, num_date){
                if(moment(date.date).diff(moment(), 'day')<0)
                    console.log('past date ');
                else
                self.from.airports.forEach(function(airport_from, num_from){
                    self.to.airports.forEach(function(airport_to, num_to){
                        $.ajax({
                            type: 'GET',
                            url: '/api/flight_search/' + airline.code +
                            '?date='+ date.date +
                            '&from='+ airport_from.airportCode+
                            '&to='+ airport_to.airportCode,
                            data: {},
                            success: function(data){
                                data = JSON.parse(data);
                                date.flights = date.flights.concat(data);
                                if(num_airline === airlines.length-1 && num_from === self.from.airports.length - 1 && num_to === self.to.airports.length - 1 && num_date === 4){
                                    callback();
                                }
                            }
                        });
                    })
                })
            })
        })
    }

}
