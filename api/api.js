var api = require('express').Router();
var request = require("request");
var flight_url = 'http://node.locomote.com/code-task';

    api.get('/airlines', function(req, res) {
        request(flight_url + '/airlines', function (error, response, body) {
            if (!error) {
                return res.json(body);
            } else {
                return res.json('Error: ' + error);
            }
        });
    });
    api.get('/airports', function(req, res) {
       request(flight_url + '/airports?q=' + req.query.q, function (error, response, body) {
            if (!error) {
                return res.json(body);
            } else {
                console.log('Error: ' + error);
            }
        });
    });
    api.get('/flight_search/:airline_code', function(req, res) {
        request(flight_url + '/flight_search/' +
                req.params.airline_code +
                '?date='+ req.query.date +
                '&from='+ req.query.from +
                '&to='+ req.query.to,
            function (error, response, body) {
            if (!error) {
                return res.json(body);
            } else {
                console.log('Error: ' + error);
            }
        });
    });

module.exports = api;